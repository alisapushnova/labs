﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static System.Console;
using lab7_1;

namespace lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Введите a.n, затем a.m:");
            RationalNumber a = new RationalNumber(int.Parse(ReadLine()), int.Parse(ReadLine()));
            WriteLine("Введите b.n, затем b.m:");
            RationalNumber b = new RationalNumber(int.Parse(ReadLine()), int.Parse(ReadLine()));
            WriteLine("int: \na = {0}\nb = {1}", (int)a, (int)b);
            WriteLine("float: \na = {0}\nb = {1}", (float)a, (float)b);
            WriteLine("double: \na = {0}\nb = {1}", (double)a, (double)b);
            WriteLine("RationalNumber: \na = {0}\nb = {1}", a, b);
            WriteLine("сумма: " + (a + b).ToString() + "\nразность: " + (a - b).ToString()
                + "\nпроизведение: " + (a * b).ToString() + "\nчастное: " + (a / b).ToString());
            if (a != b)
                WriteLine(a.ToString() + " != " + b.ToString());
            else 
                WriteLine(a.ToString() + " = " + b.ToString());
            ReadKey();
        }
    }
}
