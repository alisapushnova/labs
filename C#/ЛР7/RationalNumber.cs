﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ЛР7
{
    class RationalNumber : IComparable
    {
        private int _n;
        private int _m;
        private double _dn;
        private double _dm;

        public RationalNumber()
        {
            _n = 0;
            _m = 0;
        }

        public RationalNumber(int temp)
        {
            N = ToRationalNumber(temp).N;
            M = ToRationalNumber(temp).M;
        }

        public RationalNumber(float temp)
        {
            N = ToRationalNumber(temp).N;
            M = ToRationalNumber(temp).M;
        }

        public RationalNumber(double temp)
        {
            N = ToRationalNumber(temp).N;
            M = ToRationalNumber(temp).M;
        }

        public RationalNumber(string temp)
        {
            N = ToRationalNumber(temp).N;
            M = ToRationalNumber(temp).M;
        }

        public RationalNumber(int n, int m)
        {
            _dn = N = n;
            _dm = M = m;
        }

        public int N { get => _n; set => _n = value; }

        public int M
        {
            get => _m;
            set
            {
                if (value != 0)
                    _m = value;
                else
                    throw new DivideByZeroException();
            }
        }

        public override string ToString() => string.Format(_n + "/" + _m);

        public static implicit operator RationalNumber(float a) => ToRationalNumber(a);
        
        public static implicit operator RationalNumber(int a) => ToRationalNumber(a);

        public static implicit operator RationalNumber(double a) => ToRationalNumber(a);

        public static implicit operator float(RationalNumber a) => (float)(a._dn / a._dm);

        public static implicit operator double(RationalNumber a) => (double)(a._dn / a._dm);

        public static explicit operator int(RationalNumber a) => (a._n / a._m);

        public static RationalNumber operator +(RationalNumber a, RationalNumber b)
        {
            RationalNumber result = new RationalNumber();
            result._m = a._m * b._m;
            result._n = a._m * b._n + b._m * a._n;
            if (result._m == result._n)
            { result._m = result._n = 1; }
            return result;
        }

        public static RationalNumber operator -(RationalNumber a, RationalNumber b)
        {
            RationalNumber result = new RationalNumber();
            result._m = a._m * b._m;
            result._n = a._m * b._n - b._m * a._n;
            if (result._m == result._n)
            { result._m = result._n = 1; }
            return result;
        }

        public static RationalNumber operator *(RationalNumber a, RationalNumber b)
        {
            RationalNumber result = new RationalNumber();
            result._m = a._m * b._m;
            result._n = a._n * b._n;
            if (result._m == result._n)
            { result._m = result._n = 1; }
            return result;
        }
        
        public static RationalNumber operator /(RationalNumber a, RationalNumber b)
        {
            RationalNumber result = new RationalNumber();
            result._m = a._m * b._n;
            result._n = a._n * b._m;
            if (result._m == result._n)
            { result._m = result._n = 1; }
            return result;
        }

        public static bool operator ==(RationalNumber a, RationalNumber b)
        {
            return a.Equals(b);
            if (b is RationalNumber && b != null)
            {
                if (b._dn / b._dm ==
                    a._dn / a._dn)
                    return true;
                else
                    return false;
            }
            return false;
        }

        public static bool operator !=(RationalNumber a, RationalNumber b)
        {
            if (!(a == b))
                return true;
            else return false;
        }

        public static RationalNumber ToRationalNumber(float floatTemp)
        {
            string temp = floatTemp.ToString();
            RationalNumber result = new RationalNumber();
            result._m = 10 * temp.Length - temp.IndexOf(",");
            result._n = int.Parse(temp.Remove(temp.IndexOf(","), 1));
            return result;
        }

        public static RationalNumber ToRationalNumber(double doubleTemp)
        {
            string temp = doubleTemp.ToString();
            RationalNumber result = new RationalNumber();
            result._m = 10 * temp.Length - temp.IndexOf(",");
            result._n = int.Parse(temp.Remove(temp.IndexOf(","), 1));
            return result;
        }

        public static RationalNumber ToRationalNumber(int intTemp)
        {
            RationalNumber result = new RationalNumber(intTemp, 1);
            return result;
        }

        public static RationalNumber ToRationalNumber(string stringTemp)
        {
            RationalNumber result = new RationalNumber();
            if (stringTemp.Contains(","))
            {
                result._m = (int)Math.Pow(10, stringTemp.Length - stringTemp.IndexOf(",") - 1);
                result._n = int.Parse(stringTemp.Remove(stringTemp.IndexOf(","), 1));
                return result;
            }
            else if (stringTemp.Contains("/"))
            {
                result._m = int.Parse(stringTemp.Remove(0 , stringTemp.IndexOf("/") + 1));
                result._n = int.Parse(stringTemp.Remove(stringTemp.IndexOf("/"), stringTemp.Length - stringTemp.IndexOf("/")));
                return result;
            }
            else if (stringTemp.Contains("/") && stringTemp.Contains(","))
                throw new Exception("Некорректный ввод.");
            else
                return ToRationalNumber(int.Parse(stringTemp));
        }

        public override bool Equals(object obj)
        {
            if (obj is RationalNumber && obj != null)
            {
                RationalNumber temp = (RationalNumber)obj;
                if (temp._dn / temp._dm == temp._dn / temp._dm)
                    return true;
                else
                    return false;
            }
            return false;
        }


        public int CompareTo(object obj)
        {
            RationalNumber temp = obj as RationalNumber;
            if (temp != null)
            {
                if (this._n / this._m > temp._n / temp._m)
                    return 1;
                if (this._n / this._m < temp._n / temp._m)
                    return -1;
                else
                    return 0;
            }
            else
                throw new ArgumentException("Параметр не является объектом типа RationalNumber!");
        }
    }
}