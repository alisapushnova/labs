﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AliceInProgram
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
           InitializeComponent();
            Grid1.Visibility = Visibility.Visible;
        }

        private void historyButtonClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(
                "Алисе скучно, она вынуждена делать свои примитивные лабы... " +
                "Остается только мечтать о чем-то более масштабном. " +
                "Если хочешь помочь Алисе сделать интересный проект, то нажми на PLAY.");
        }

        private void playButtonClick(object sender, RoutedEventArgs e)
        {
            Grid1.Visibility = Visibility.Collapsed;
            Grid2.Visibility = Visibility.Visible;
            var file = new FileInfo("Images\\screen2.jpg");
            screen2.Source = new BitmapImage(new Uri(file.FullName));
        }

        private void getGifkaClick(object sender, RoutedEventArgs e)
        {
            Grid3.Visibility = Visibility.Visible;
            Grid2.Visibility = Visibility.Collapsed;
        }

        private void nextButtonClick(object sender, RoutedEventArgs e)
        {
            Grid4.Visibility = Visibility.Visible;
            Grid3.Visibility = Visibility.Collapsed;
        }

        private void okButtonClick(object sender, RoutedEventArgs e)
        {
            BigOrSmall.Visibility = Visibility.Collapsed;
            okButton.Visibility = Visibility.Collapsed;
            Grid4.Visibility = Visibility.Collapsed;
            Grid5.Visibility = Visibility.Visible;
        }

        private void bottleButtonClick(object sender, RoutedEventArgs e)
        {
            AliceImage.Width = 455;
            AliceImage.Height = 428;
        }

        private void cookieButtonClick(object sender, RoutedEventArgs e)
        {
            AliceImage.Width = 155;
            AliceImage.Height = 168;
        }

        private void chipButtonClick(object sender, RoutedEventArgs e)
        {
            Grid5.Visibility = Visibility.Collapsed;
            Grid6.Visibility = Visibility.Visible;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Grid6.Visibility = Visibility.Collapsed;
            Grid7.Visibility = Visibility.Visible;
            EditWithAnswer.Focus();
        }

        private List<string> PossibleVariants = new List<string>() { "1", "2", "3" };

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!PossibleVariants.Any(EditWithAnswer.Text.Contains))
                return;
            
            if (EditWithAnswer.Text == "3")
            {
                new Thread(() =>
                {
                    MessageBox.Show("Поздравляю!");
                }).Start();
                Grid7.Visibility = Visibility.Collapsed;
                Grid8.Visibility = Visibility.Visible;
            }
            else 
            {
                MessageBox.Show("Неудача!");
            }
        }
    }
}

/* public interface IPerson
        {
            //описание все методы и свйоства. Движения, Рост, Вес и тд.
            void Move();

            void IncImage();
            void DecImage();
            int Age { get; set; }
            int Height { get; set; }

        }

   
        public class Person : IPerson
        {
            event EventHandler AgeChanged;

            //реализую все методы и св-ва твоего интерфейса

            public void DecImage()
            {
                //уменьшаем картинку 
            }

            public void IncImage()
            {
                //увеличиваем картинку
            }

            public void Move()
            {
                //Двигаемся
            }


            private int age = 0;
            public int Age
            {
                get => age;
                set
                {
                    try
                    {
                        if (age < 0)
                        {
                            throw new Exception();
                        }

                        AgeChanged?.Invoke(null, new EventArgs());
                        age = value;
                    }
                    catch (Exception e)
                    {

                        age = 0;
                    }
                 
                }
            }
            public int Height { get; set; }
        }


        public class Main
        {
            public Main()
            {
                var Alice = new Person
                {
                    Age = 22,
                    Height = 170
                };
                Alice.Move();
                Alice.Age++;
                Alice.IncImage();
            }
        }*/
