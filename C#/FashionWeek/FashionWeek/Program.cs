﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FashionWeek
{

    class Person
    {
        public string Sex { get; set; }
        public int Age { get; set; }
        public string Race { get; set; }


        public Person(string sex, int age, string race)
        {
            Sex = sex;
            Age = age;
            Race = race;
        }

        public void GetInfo()
        {
            Console.WriteLine($"Пол: {Sex} \nВозраст: {Age}  \nРаса: {Race}");
        }
    }

        class Model : Person
        {
            // Статическое поле
            public static int MaxAvaliableAgeForWorkOnPodium => 90;

        public Model(string sex, int age, string race, double weight, double height) : base(sex, age, race)
            {
                Weight = weight;
                Height = height;
            }

            // Перегруженный метод с сокрытием реализации родительского класса
            public new void GetInfo()
            {
                Console.WriteLine($"Пол: {Sex} \nВозраст: {Age} \nРаса: {Race} \nРост: {Height} \nВес: {Weight}");
            }

            // Инкапсуляция
            private double _weight;
            public double Weight
            {
                get => _weight;
                set => _weight = value > 0 ? value : 0;
            }

            private double _height;
            public double Height
            {
                get { return _height; }
                set { _height = value > 0 ? value : 0; }
            }
        }

        class Designer : Person
        {
            // Статическое поле
            public static int MinWorkExp => 5;

            public Designer(int age, string sex, string race, int workExperience, DesignerProfessionType professionType) : base(sex, age, race)
            {
                Experience = workExperience;
                ProfessionType = professionType;
            }

            // Перегруженный метод с сокрытием реализации родительского класса
            public new void GetInfo()
            {
                Console.WriteLine(
                    $"Возраст: {Age} \n" +
                    $"Пол: {Sex} \n" +
                    $"Опыт работы: {Experience} \n" +
                    $"Специализация: {ProfessionType} \n" +
                    $"Раса: {Race}");
            }

            // Инкапсуляция
            private double _experience;
            public double Experience
            {
                get => _experience;
                // Использования статического поля
                set => _experience = value > MinWorkExp ? value : MinWorkExp;
            }

            public DesignerProfessionType ProfessionType { get; set; }

            public enum DesignerProfessionType
            {
                Reference, Model, Clothes, Banner
            }
        }



    class Program
    {
        static void Main(string[] args)
        {   
            List<Model> Information = new List<Model>();

            string input = @"/Users/alisapushnova/Desktop/input.txt";

            try
            {
                for (; ; )
                {
                    Console.WriteLine("\nВыберите, что хотите сделать: \n\t1. Всю информацию о моделях" +
                                           "\n\t2. Считать данные из файла  \n\t3. Добавить модель \n\t4. Выход\n");
                    int number = int.Parse(Console.ReadLine());

                    switch (number)
                    {
                        case 1:
                            Console.WriteLine("Все наши модели:\n");
                            for (int i = 0; i < Information.Count; i++)
                            {
                                Information[i].GetInfo();
                            }
                        break;

                        case 2:
                            using (StreamReader sr = new StreamReader(input))
                            {

                                for (int i = 0; i < 12; i++)
                                {
                                    string help = sr.ReadLine();
                                    string[] items = help.Split(' ');

                                    string Sex = items[0];
                                    int Age = Convert.ToInt32(items[1]);
                                    string Race = items[2];
                                    double Weight = Convert.ToDouble(items[3]);
                                    double Height = Convert.ToDouble(items[4]);

                                    var model = new Model(Sex, Age, Race, Weight, Height);

                                    Information.Add(model);

                                }
                                Console.WriteLine("\nИнформация добавлена\n");
                            }
                            break;

                        case 3:
                            Console.WriteLine("Введите пол: ");
                            string SexNew = Console.ReadLine();

                            Console.WriteLine("Введите возраст: ");
                            int AgeNew = Convert.ToInt32(Console.ReadLine());

                            Console.WriteLine("Введите расу: ");
                            string RaceNew = Console.ReadLine();

                            Console.WriteLine("Введите вес: ");
                            double WeightNew = Convert.ToDouble(Console.ReadLine());

                            Console.WriteLine("Введите рост: ");
                            double HeightNew = Convert.ToDouble(Console.ReadLine());

                            var modelNew = new Model(SexNew, AgeNew, RaceNew, WeightNew, HeightNew);
                            Information.Add(modelNew);

                            break;

                        case 4:
                            Console.WriteLine("Для выхода нажмите любую клавишу");
                            Console.ReadKey();
                            return;

                        default:
                            Console.WriteLine("Введите цифру от 1 до 5");
                            break;
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("e.Message");
            }

        }
    }



}
