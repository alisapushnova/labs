//
//  main.c
//  lab1
//
//  Created by Пушнова Алиса Сергеевна on 2/19/19.
//  Copyright © 2019 Пушнова Алиса Сергеевна. All rights reserved.
//  Любая целочисленная денежная сумма s > 7 рублей может быть выдана без сдачи «трешками» и «пятерками».
//  Найти для заданной суммы s необходимое количество «трешек» и «пятерок».

#include <stdio.h>

int main (int argc, const char * argv[])
{
    int summa, result;
    int help_for_5, help_for_3;
    int answer_for_5, answer_for_3;

    printf("Введите денежную сумму: ");
    scanf("%i", &summa);
    
    if (summa < 7)
    {
        printf("Введите сумму больше 7!\n");
    }
    else
    {
        if (summa % 3 == 0 && summa % 5 != 0)
        {
            result = summa / 3.0;
            printf("\nНеобходимое количество «трешек» - %i\n", result);

        }
    
        if (summa % 5 == 0 && summa % 3 != 0)
        {
            result = summa / 5.0;
            printf("\nНеобходимое количество «пятерок» - %i", result);
            printf("\n");
        }
    
        if (summa % 5 == 0 && summa % 3 == 0)
        {
            answer_for_5 = summa / 5.0;
            printf("\nНеобходимое количество «пятерок» - %i\n", answer_for_5);
        
            answer_for_3 = summa / 3.0;
            printf("\nЛибо же необходимое количество «троек» - %i\n", answer_for_3);
          
        }
        
        help_for_5 = summa - 5;
        if (help_for_5 % 3 == 0)
        {
            result = help_for_5 / 3.0;
            printf("\nНеобходимое количество «пятерок» - 1 и необходимое количество «троек» - %i\n", result);
        }
        
        help_for_3 = summa - 3;
        if (help_for_3 % 5 == 0)
        {
            result = help_for_3 / 5.0;
            printf("\nНеобходимое количество «троек» - 1 и необходимое количество «пятерок» - %i\n", result);
        }
    }
    
    return 0;
}
