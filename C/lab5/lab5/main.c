//
//  main.c
//  lab5
//
//  Created by Пушнова Алиса Сергеевна on 4/14/19.
//  Copyright © 2019 Пушнова Алиса Сергеевна. All rights reserved.
//

/* Наишите программу копирования одного стека в другой. */

#include <stdio.h>

#include "stack.h"

int main(void)
{
    STACK_HANDLE stack1;
    STACK_HANDLE stack2;
    
    int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    
    int i = 0;
    
    StackCreate(&stack1, sizeof(int));
    StackCreate(&stack2, sizeof(int));
    
    for (i = 0; i < sizeof(a) / sizeof(*a); i++)
    {
        StackPush(stack1, &a[i]);
    }
    
    StackCopy(stack2, stack1);
    
    for (i = 0; i < sizeof(a) / sizeof(*a); i++)
    {
        StackPop(stack2, &a[i]);
    }
    
    for (i = 0; i < sizeof(a) / sizeof(*a); i++)
    {
        printf("%d ", a[i]);
    }
    
    StackDelete(&stack1);
    StackDelete(&stack2);
    
    return 0;
}
