//
//  stack.h
//  lab5
//
//  Created by Пушнова Алиса Сергеевна on 4/14/19.
//  Copyright © 2019 Пушнова Алиса Сергеевна. All rights reserved.
//

#ifndef STACK_H
#define STACK_H

#include <stdio.h>

typedef struct TStack* STACK_HANDLE;

enum
{
    stack_no_error,
    stack_error_mem_alloc,
    stack_error_invalid_handle,
    stack_error_invalid_argument,
    stack_error_empty_stack
};

int StackCreate(STACK_HANDLE* stack_handle, const size_t stack_element_size);
void StackDelete(STACK_HANDLE* stack_handle);
int IsStackEmpty(STACK_HANDLE stack_handle);
int StackPush(STACK_HANDLE stack_handle, const void* const element_address);
int StackPop(STACK_HANDLE stack_handle, void* const elementAddress);
int StackCopy(STACK_HANDLE destination_stack_handle, STACK_HANDLE source_stack_handle);

#endif 
