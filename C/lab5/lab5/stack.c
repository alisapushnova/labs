//
//  stack.c
//  lab5
//
//  Created by Пушнова Алиса Сергеевна on 4/14/19.
//  Copyright © 2019 Пушнова Алиса Сергеевна. All rights reserved.
//

#include <stdlib.h>
#include <string.h>

#include "stack.h"

#define STACK_DEFAULT_ALLOC_LENGTH 100

struct TStack
{
    void* elements;         /* массив данных, где каждый элемент имеет размер 'element_size' */
    size_t element_size;     /* размер каждого элемента, который содержит стек */
    size_t stack_length;     /* длина стека, количество предметов */
    size_t allocated_length; /* количество выделенных элементов для стека */
};

/*
 Создает стек по заданному указателю на дескриптор стека.
 Выделяет память для структуры и массив размера по умолчанию для элементов заданного размера.
 */

int StackCreate(STACK_HANDLE* h_stack_ptr, const size_t stack_element_size)
{
    int error_code = stack_no_error;
    
    do
    {
        if ((h_stack_ptr == NULL) || (stack_element_size == 0))
        {
            error_code = stack_error_invalid_argument; // invalid - недействительный
            break;
        }
        
        (*h_stack_ptr) = malloc(sizeof(**h_stack_ptr));
        
        // продумай случай, когда функция не может выделить память для чего-либо
        
        if ((*h_stack_ptr) == NULL)
        {
            error_code = stack_error_mem_alloc;
            break;
        }
        
        (*h_stack_ptr)->elements = malloc(STACK_DEFAULT_ALLOC_LENGTH * stack_element_size);
        
        if ((*h_stack_ptr)->elements == NULL)
        {
            error_code = stack_error_mem_alloc;
            StackDelete(h_stack_ptr);
            break;
        }
        else
        {
            (*h_stack_ptr)->allocated_length = STACK_DEFAULT_ALLOC_LENGTH;
            (*h_stack_ptr)->element_size = stack_element_size;
            (*h_stack_ptr)->stack_length = 0;
        }
    }
    while (0);
    
    return error_code;
}

/*
 Выделение памяти для заданного h_stack_ptr, уничтожение стека.
 Дескриптор можно использовать повторно, вызывая «StackCreate».
 */

void StackDelete(STACK_HANDLE* h_stack_ptr)
{
    if ((h_stack_ptr != NULL) && ((*h_stack_ptr) != NULL))
    {
        free((*h_stack_ptr)->elements);
        free(*h_stack_ptr);
        (*h_stack_ptr) = NULL;
    }
}

/* Проверяет, является ли данный стек пустым дескриптором (содержит ноль элементов) */

int IsStackEmpty(STACK_HANDLE h_stack) // empty - пустой
{
    if (h_stack != NULL)
    {
        return (h_stack->stack_length == 0);
    }
    return 0;
}


int StackPush(STACK_HANDLE h_stack, const void* const element_address)
{
    int error_code = stack_no_error;
    
    void* temp_ptr = NULL;
    
    do
    {
        if (h_stack == NULL)
        {
            error_code = stack_error_invalid_handle;
            break;
        }
        
        if (element_address == NULL)
        {
            error_code = stack_error_invalid_argument;
            break;
        }
        
        if (h_stack->allocated_length == h_stack->stack_length)
        {
            /* изменение размера стека для хранения в два раза больше элементов  */
            h_stack->allocated_length = h_stack->allocated_length * 2;
            temp_ptr = realloc(h_stack->elements, h_stack->allocated_length * h_stack->element_size);
            
            if (temp_ptr == NULL)
            {
                error_code = stack_error_mem_alloc;
                break;
            }
            
            h_stack->elements = temp_ptr;
            temp_ptr = NULL;
        }
        
        /* расчет адреса для сохранения данных */
        temp_ptr = (char*)h_stack->elements + h_stack->stack_length * h_stack->element_size;
        
        /* копирование данных */
        memcpy(temp_ptr, element_address, h_stack->element_size);
        h_stack->stack_length++;
    }
    while (0);
    return error_code;
}

int StackPop(STACK_HANDLE h_stack, void* const element_address)
{
    int error_code = stack_no_error;
    
    const void* source_address = NULL;
    
    if (h_stack == NULL)
    {
        error_code = stack_error_invalid_handle;
    }
    else if (element_address == NULL)
    {
        error_code = stack_error_invalid_argument;
    }
    else if (IsStackEmpty(h_stack))
    {
        error_code = stack_error_empty_stack;
    }
    else
    {
        h_stack->stack_length--;
        source_address = (const char*)h_stack->elements + h_stack->stack_length * h_stack->element_size;
        memcpy(element_address, source_address, h_stack->element_size);
    }
    return error_code;
}

/*
 Делает полную копию стека.
 Обе handles должны быть действительными.
 
 Возвращаемое значение:
 в случае успеха возвращает 'stack_no_error' или:
   - 'stack_error_invalid_handle': если указан неверный дескриптор
   - 'stack_error_invalid_argument': если 'elementAddress' равен NULL
   - 'stack_error_mem_alloc': если функция не может выделить память для чего-либо
   - 'stack_error_empty_stack': если исходный стек пуст
 */

int StackCopy(STACK_HANDLE h_destination_stack, STACK_HANDLE h_source_stack)
{
    int error_code = stack_no_error;
    
    void* temp_ptr = NULL;
    
    do
    {
        if ((h_destination_stack == NULL) || (h_source_stack == NULL))
        {
            error_code = stack_error_invalid_handle;
            break;
        }
        
        if ((h_destination_stack->element_size != h_source_stack->element_size) ||
            !IsStackEmpty(h_destination_stack))
        {
            error_code = stack_error_invalid_argument;
            break;
        }
        
        if (IsStackEmpty(h_source_stack))
        {
            error_code = stack_error_empty_stack;
            break;
        }
        
        if (h_destination_stack->allocated_length != h_source_stack->allocated_length)
        {
            /* изменение размера места назначения в соответствии с источником */
            temp_ptr = realloc(h_destination_stack->elements, h_source_stack->stack_length * h_source_stack->element_size);
            if (temp_ptr == NULL)
            {
                error_code = stack_error_mem_alloc;
                break;
            }
            
            h_destination_stack->elements = temp_ptr;
            temp_ptr = NULL;
        }
        
        h_destination_stack->allocated_length = h_source_stack->allocated_length;
        h_destination_stack->stack_length = h_source_stack->stack_length;
        memcpy(h_destination_stack->elements, h_source_stack->elements, h_source_stack->stack_length * h_source_stack->element_size);
    }
    while (0);
    return error_code;
}
