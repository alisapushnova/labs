//
//  main.c
//  lab3
//
//  Created by Пушнова Алиса Сергеевна on 3/20/19.
//  Copyright © 2019 Пушнова Алиса Сергеевна. All rights reserved.

#include <stdio.h>
#include <stdlib.h>

void FindingGleams()
{
    int ***cube;
    int iNew, jNew, kNew, i, j, k, n, num = rand() % 20, count = 0;
    printf("Введите размер куба: ");
    scanf("%d", &n);
    
    cube = (int***)malloc(n * sizeof(int**));
    for (i = 0; i < n; i++)
    {
        cube[i] = (int**)malloc(n * sizeof(int*));
        for (j = 0; j < n; j++)
        {
            cube[i][j] = (int*)malloc(n * sizeof(int));
            for (k = 0; k < n; k++)
            {
                cube[i][j][k] = 1;
                // printf("cube[%d][%d][%d] = %d\n",i, j, k, cube[i][j][k]);
            }
        }
    }
    
    for (i = 0; i < num; i++)
    {
        printf("Координата просвета по X: %d", iNew = rand() % n);
        printf("\nКоордината просвета по Y: %d", jNew = rand() % n);
        printf("\nКоордината просвета по Z: %d\n\n", kNew = rand() % n);
        cube[iNew][jNew][kNew] = 0;
    }
    
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            for (k = 0; k < n; k++)
            {
                if (cube[i][j][k] == 0)
                {
                    count++;
                }
            }
        }
    }
    
    printf("\nКоличество просветов: %d\n", count);
}
int main()
{
    FindingGleams();
    return 0;
}
