//
//  main.c
//  laba 6
//
//  Created by Пушнова Алиса Сергеевна on 4/27/19.
//  Copyright © 2019 Пушнова Алиса Сергеевна. All rights reserved.

//  "В текстовом файле записаны целые числа. Построить бинарное дерево поиска, в узлах которого хранятся числа из файла. Разработать функцию, определяющую количество нечетных элементов."

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define buf_size 10

typedef enum { left, right } direction;

// Структура, описывающая узел дерева
typedef struct item
{
    int data;
    struct item *left;
    struct item *right;
} Item;

// Структура, описывающая корень дерева
typedef struct
{
    Item *root;
} Tree;

void InThisTree(Tree *tree)
{
    tree->root = NULL;
}

void InThisNode(Item *node, int data)
{
    node->data = data;
    node->left = NULL;
    node->right = NULL;
}

void Push(Tree *tree, int data)
{
    direction dir;
    if (tree->root == NULL)
    {
        tree->root = (Item*)malloc(sizeof(Item));
        InThisNode(tree->root, data);
        return;
    }
    
    Item *previous, *current = tree->root;
    do
    {
        previous = current;
        if (data < previous->data)
        {
            current = current->left;
            dir = left;
        }
        else
        {
            current = current->right;
            dir = right;
        }
    } while (current != NULL);
    
    if (dir == left)
    {
        previous->left = (Item*)malloc(sizeof(Item));
        InThisNode(previous->left, data);
    }
    else
    {
        previous->right = (Item*)malloc(sizeof(Item));
        InThisNode(previous->right, data);
    }
}

void DownloadFile(Tree *tree, const char *filename)
{
    char buffer[buf_size];
    memset(buffer, 0, buf_size * sizeof(char));
    FILE * file;
    file = fopen("/Users/alisapushnova/Documents/БГУИР/C/lab6/laba 6/file.txt", "r+");
    // выйти, если файл не удалось открыть
    if (!file)
        exit(1);
    // Считываем значения из файла построчно
    while (fgets(buffer, buf_size, file) != NULL)
        Push(tree, atoi(buffer)); // и добавляем в дерево
    fclose(file);
}

// Разработать функцию, определяющую количество нечетных элементов
void FindOddNumbers(Item *node, int *number)
{
    if (node == NULL)
        return;
    if (node->data % 2 == 1)
        (*number)++;
    FindOddNumbers(node->left, number);
    FindOddNumbers(node->right, number);
}

int main()
{
    Tree tree;
    int odd_numbers = 0;
    const char *filename = "/Users/alisapushnova/Documents/БГУИР/C/lab6/laba 6/file.txt";
    InThisTree(&tree);
    DownloadFile(&tree, filename);
    FindOddNumbers(tree.root, &odd_numbers);
    printf("Количество нечетных элементов: %d\n", odd_numbers);
    return 0;
}




