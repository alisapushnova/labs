#include <stdio.h>
#include <math.h>

int main ()
{
    float radius, square, angle;
    int n;
    printf("Введите радиус круга: ");
    scanf("%f", &radius);
    
    for (;;)
    {
        printf("\nВыберите действие: \n\t1.Заменить радиус\n\t2.Вывести площадь\n\t3.Вывести площадь сектора (укажите угол а)\n\t4.Вывести длину окружности\n\t5.Вывести сторону вписанного квадрата\n\t6.Вывести площадь конуса\n\t7.Информация о версии и авторе\n\t8.Выход\n");
        scanf("%d", &n);
        switch (n)
        {
            case 1:
                printf("Введите новое значение радиуса: ");
                scanf("%f", &radius);
                break;
            case 2:
                square = M_PI * radius * radius;
                printf("\nПлощадь = %f\n", square);
                break;
            case 3:
                printf("Введите угол в радианах: ");
                scanf("%f", &angle);
                square = angle * powl(radius, 2) / 2;
                printf("\nПлощадь сектора = %f\n", square);
                break;
            case 4:
                square = 2 * M_PI * radius;
                printf("\nДлина окружности = %f\n", square);
                break;
            case 5:
                square = sqrt(2) * radius;
                printf("\nCторона вписанного квадрата = %f\n", square);
                break;
            case 6:
                square = M_PI * powl(radius, 3) / 3;
                printf("\nПлощадь конуса = %f\n", square);
                break;
            case 7:
                printf("\nПрограмма создана Пушновой Алисой Сергеевной группа 853505 \nПрограмма v1.0.0\n");
                break;
            default:
                printf("\n! Введено неверное значение, повторите ввод !\n");
        }
    }
    return 0;
}
