//
//  main.c
//  task_2
//
//  Created by Пушнова Алиса Сергеевна on 3/7/19.
//  Copyright © 2019 Пушнова Алиса Сергеевна. All rights reserved.

//  Численно убедиться в справедливости равенства, для чего для заданного значения х вычислить его
//  левую часть и разложение, стоящее в правой части. При каком n исследуемое выражение отличается от sin x
//  менее, чем на заданную погрешность ε? Испытать разложение на сходимость при разных значениях х.

#include <stdio.h>
#include <math.h>
#include <time.h>

    /* Функция clock() возвращает время, пройденное с момента запуска программы, вызвавшей фун­кцию clock(). Если часы недоступны, то возвращается —1. Для преобразования возвращаемой ве­личины в секунды надо разделить ее на макрос CLK_TCK. */

double decomposition_sin(int n, double x, double sum)
{
    int factorial = 1;
    if (n > 0)
    {
        sum += pow(-1, n - 1) * pow(x, 2 * n - 1) / factorial;
        factorial *= (2 * n + 1) * 2 * n;
        return decomposition_sin(n - 1, x, sum);
    }
    else
        return sum;
}

int main()
{
    int n = 0;
    long double epsilon;
    long double x;
    long double sum = 0;
    int factorial_another = 1;
    
    printf("Введите x = ");
    scanf("%Lf", &x);
    x = x * M_PI / 180;
    printf("Введите Эпсилон: ");
    scanf("%Lf", &epsilon);
    
    
    clock_t begini = clock();
    for (int i = 1; i < 1000000; i++)
    {
        sum = 0.0;
        factorial_another = 1;
    
        for (n = 1; fabsl(sum - sin(x)) > epsilon; n++)
        {
            sum += pow(-1, n - 1) * pow(x, 2 * n - 1) / factorial_another;
            factorial_another *= (2 * n + 1) * 2 * n;
        }
    }
    clock_t endi = clock();
    
    printf("\nИтерация: ");
    printf("\nОтвет = %.9Lf", sum);
    printf("\nЭлипсон меньше чем %.9Lf, когда n = %d\n", epsilon, n);
    double time_spenti = (double)(endi - begini) / CLOCKS_PER_SEC;
    printf("Время затраченное на программу:\t%.3f сек.", time_spenti);
   
    sum = 0.0;
    factorial_another = 1;
    clock_t beginr = clock();
    for (int i = 1; i < 1000000; i++)
        sum = decomposition_sin(n, x, 0);
    clock_t endr = clock();
    
    printf("\n\nРекурсия: ");
    printf("\nОтвет = %.9Lf", sum);
    printf("\nЭлипсон меньше чем %.9Lf, когда n = %d\n", epsilon, n);
    double time_spentr = (double)(endr - beginr) / CLOCKS_PER_SEC;
    printf("Время затраченное на программу:\t%.3f сек.", time_spentr);
    printf("\n\n");
    
    return 0;
}
    
   
