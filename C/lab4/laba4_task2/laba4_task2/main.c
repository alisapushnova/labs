//
//  main.c
//  laba4_task2
//
//  Created by Пушнова Алиса Сергеевна on 3/28/19.
//  Copyright © 2019 Пушнова Алиса Сергеевна. All rights reserved.
//

/* Текстовый файл состоит из 40-байтовых строк. Последние 8 байтов строки содержат номер строки
 (в случайном порядке). Упорядочить строки в файле по номерам. */

#include <stdio.h>
#include <string.h>
#include <math.h>

int FindNumber(char * string);
void SortBubble(int countOfRaws, int * arr);

int main(void)
{
    int N = INT32_MAX, countOfRaws = 0;
    char * buffer = (char * ) malloc(sizeof(char) * N);
    FILE * fp;
    fp = fopen("/Users/alisapushnova/Documents/БГУИР/C/lab4/laba4_task2/laba4_task2/file.txt", "r+");
    if (!fp)
        exit(1);
    
    while (fgets(buffer, N, fp) != NULL)
        countOfRaws++;
    //printf("%d", countOfRaws);
    char ** text = (char ** ) malloc(sizeof(char * ) * countOfRaws);
    char ** newText = (char ** ) malloc(sizeof(char * ) * countOfRaws);
    
    for (int i = 0; i < countOfRaws; i++)
    {
        text[i] = (char * ) malloc(sizeof(char * ) * 40);
        newText[i] = (char * ) malloc(sizeof(char * ) * 40);
    }
    
    int i = 0;
    fseek(fp, 0, SEEK_SET);
    
    while (fgets(buffer, N, fp) != NULL)
    {
        strcpy(text[i], buffer);
        i++;
    }
    
    printf("Текст в файле: \n");
    for (int i = 0; i < countOfRaws; i++)
    {
        printf("%s", text[i]);
    }
    
    int * arr = (int * ) malloc(sizeof(int) * countOfRaws);
    
    for (int i = 0; i < countOfRaws; i++)
    {
        arr[i] = FindNumber(text[i]);
    }
    
    SortBubble(countOfRaws, arr);
    
    for (int i = 0; i < countOfRaws; i++)
    {
        int j = arr[i];
        
        for (int k = 0; k < countOfRaws; k++)
        {
            if (j == FindNumber(text[k]))
            {
                j = k;
                break;
            }
        }
        
        if (newText[i] == NULL)
        {
            printf("\nОшибка в файле\nЗавершение программы...\n\n");
            exit(1);
        }
        strcpy(newText[i], text[j]);
    }
    
    fseek(fp, 0, SEEK_SET);
    printf("\nИзменённый текст в файле: \n");
    
    for (int i = 0; i < countOfRaws; i++)
    {
        fprintf(fp, "%s", & newText[i]['\0']);
        printf("%s", newText[i]);
    }
    fclose(fp);
    return 0;
}

int FindNumber(char * string)
{
    int number = 0, count = 0, position = 0;
    for (int i = 0; i < 8; i++)
    {
        if (string[32 + i] >= '0' && string[32 + i] <= '9')
        {
            count++;
            position = i;
        } else if (count != 0)
        {
            break;
        }
    }
    for (int i = 0; i < count; i++)
    {
        number += (string[32 + position - count + i + 1] - '0') * pow(10, count - i - 1);
    }
    return number;
}

void SortBubble(int countOfRaws, int * arr)
{
    for (int i = 0; i < countOfRaws - 1; i++)
    {
        for (int j = 0; j < countOfRaws - i - 1; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                int tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
            }
        }
    }
}
