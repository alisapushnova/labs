//---------------------------------------------------------------------------
#ifndef ContainerH
#define ContainerH

#include "Book.h"
#include <algorithm>
#include <cstring>
#include "string"
//---------------------------------------------------------------------------
class BookList
{
	Book *Books;
	int count;

public:
	BookList(int n) {
		Books = new Book[n];
		count = 0;
	}

	~BookList() {
		delete[]Books;
	}

	void OutAll(TMemo *Memo1)
	{
		Memo1 -> Clear();
		for (int i = 0; i < count; i++)
		{
			Memo1->Lines->Text = Memo1->Lines->Text + Books[i].GetString();
		}
	}

	void Out(TMemo *Memo1, int i)
	{
		Memo1->Lines->Text = Memo1->Lines->Text + Books[i].GetString();
	}

    	void Add(Book book)
	{
		Books[count] = book;
		count++;
	}

//	void Add(Book *book)
//	{
//		Books[count] = *book;
//		count++;
//	}

	void SearchSurname(TEdit* Edit1,TMemo* Memo1)
	{
		String sur = Edit1 -> Text;
		for (int i = 0; i < count; i++)
		{
		   if (Books[i].surname == sur)
		   {
				Out(Memo1, i);
		   }
		}
	}

	void SearchTitle(TEdit* Edit1,TMemo* Memo1)
	{
		int e = 0,indx = -1;
		String tit =  Edit1 -> Text;
		for (int i = 0; i < count; i++)
		{
		   if (Books[i].title == tit)
		   {
				Out(Memo1, i);
		   }
		}
	}

	void SearchPublishing(TEdit* EditPublishing,TMemo* Memo1)
	{
		String pub =  EditPublishing -> Text;
		for (int i = 0; i < count; i++)
		{
		   if (Books[i].publishing == pub)
		   {
				Out(Memo1, i);
		   }
		}
	}


	void SortAllBooksYear()
	{
	 std::sort(Books, Books + count,
		  [](Book & a, Book & b) -> bool
		  { return a.GetYear() > b.GetYear(); } );
	}


	void AddNew(Book book)
	{
		Books[count] = book;
		count++;
	}

	String GetNewSurname(int j)
	{
		return Books[j].GetSurname();
	}
	String GetNewTitle(int j)
	{
		return Books[j].GetTitle();
	}
	String GetNewYear(int j)
	{
		return Books[j].GetYear();
	}
	String GetNewPublishing(int j)
	{
		return Books[j].GetPublishing();
	}
	String GetNewPages(int j)
	{
		return Books[j].GetPages();
	}
	int GetCount()
	{
		return count;
	}
};

#endif
