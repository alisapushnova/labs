﻿// ---------------------------------------------------------------------------
#include <cstring>

#ifndef BookH
#define BookH

// ---------------------------------------------------------------------------

class Book
{
public:
	String surname;
	String title;
	int year;
	String publishing;
	int pages;

public:
	Book(){
	}

	void SetSurname(String sur) {
		surname = sur;
	}

	String GetSurname() {
		return surname;
	}

	void SetTitle(String t) {
		title = t;
	}

	String GetTitle() {
		return (title);
	}

	void SetYear(int y) {
		year = y;
	}

	int GetYear() {
		return (year);
	}

	void SetPublishing(String pub) {
		publishing = pub;
	}

	String GetPublishing() {
		return (publishing);
	}

	void SetPages(int pg) {
		pages = pg;
	}

	int GetPages() {
		return (pages);
	}

	String GetString() {
		return "Автор: " + GetSurname() + "\nНазвание: " + GetTitle() +
			"\nГод издания: " + IntToStr(GetYear()) + "\nИздательство: " +
			GetPublishing() + "\nКоличество страниц: " +
			IntToStr(GetPages()) + "\n\n";
	}
};

#endif

