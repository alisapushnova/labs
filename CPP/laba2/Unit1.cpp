//---------------------------------------------------------------------------

#include <vcl.h>
#include <cstring>
#include <algorithm>
#pragma hdrstop

#include "Book.h"
#include "Unit1.h"
#include "Container.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
BookList *Bk;
//AnsiString SFile = "f.txt";
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	ComboBox1->Items->Insert(0,"�������");
	ComboBox1->Items->Insert(1,"��������");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonOpenLibClick(TObject *Sender)
{
	Bk = new BookList(10);

	Memo1->Lines->LoadFromFile(OpenDialog1 -> FileName) ;

	for (int c = -1; c < Memo1->Lines->Count-1; )
	{
		Book *book = new Book();

		book->SetSurname(Memo1->Lines->Strings[++c]);

		book->SetTitle(Memo1->Lines->Strings[++c]);

		book->SetYear(StrToInt(Memo1->Lines->Strings[++c]));

		book->SetPublishing(Memo1->Lines->Strings[++c]);

		book->SetPages(StrToInt(Memo1->Lines->Strings[++c]));

		Bk->Add(*book);
	}
	// Memo1 -> Clear();
	Bk -> OutAll(Memo1);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonExitClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonSearchClick(TObject *Sender)
{
	Memo1 -> Clear();
	if (ComboBox1->ItemIndex == 0)
	{
	   Bk -> SearchSurname(Edit1, Memo1);
	}  else if (ComboBox1->ItemIndex == 1)
			{
			  Bk -> SearchTitle(Edit1, Memo1);
			}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonSortClick(TObject *Sender)
{
	Bk -> SortAllBooksYear();
	Bk -> OutAll(Memo1);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonBooksClick(TObject *Sender)
{
	Memo1 -> Clear();
	Bk -> SearchPublishing(EditPublishing, Memo1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonAddClick(TObject *Sender)
{
	Book *book = new Book();

	book->SetSurname(AddSurname->Text);
	book->SetTitle(AddTitle->Text);
	book->SetYear(StrToInt(AddYear->Text));
	book->SetPublishing(AddPublishing->Text);
	book->SetPages(StrToInt(AddPages->Text));

	Bk->AddNew(*book);

	Bk->Out(Memo1, Bk->GetCount() - 1);

	Memo1->Lines->SaveToFile(SaveDialog1 -> FileName) ;
}
//---------------------------------------------------------------------------

