//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *ButtonAdd;
	TButton *ButtonSort;
	TButton *ButtonSearch;
	TButton *ButtonExit;
	TMemo *Memo1;
	TButton *ButtonOpenLib;
	TLabel *Label1;
	TPanel *PanelAdd;
	TEdit *AddSurname;
	TLabel *Label2;
	TEdit *AddTitle;
	TEdit *AddYear;
	TEdit *AddPublishing;
	TEdit *AddPages;
	TPanel *PanelSearch;
	TEdit *Edit1;
	TButton *ButtonBooks;
	TComboBox *ComboBox1;
	TSaveDialog *SaveDialog1;
	TOpenDialog *OpenDialog1;
	TEdit *EditPublishing;
	TPanel *PanelOnePublishing;
	void __fastcall ButtonOpenLibClick(TObject *Sender);
	void __fastcall ButtonExitClick(TObject *Sender);
	void __fastcall ButtonSearchClick(TObject *Sender);
	void __fastcall ButtonSortClick(TObject *Sender);
	void __fastcall ButtonBooksClick(TObject *Sender);
	void __fastcall ButtonAddClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
