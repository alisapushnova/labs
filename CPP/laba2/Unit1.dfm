object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 403
  ClientWidth = 760
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 88
    Top = 48
    Width = 3
    Height = 13
  end
  object ButtonSort: TButton
    Left = 497
    Top = 293
    Width = 168
    Height = 31
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    TabOrder = 0
    OnClick = ButtonSortClick
  end
  object ButtonExit: TButton
    Left = 640
    Top = 370
    Width = 112
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 1
    OnClick = ButtonExitClick
  end
  object Memo1: TMemo
    Left = 296
    Top = 18
    Width = 369
    Height = 247
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object ButtonOpenLib: TButton
    Left = 56
    Top = 13
    Width = 184
    Height = 29
    Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1091
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 3
    OnClick = ButtonOpenLibClick
  end
  object PanelAdd: TPanel
    Left = 24
    Top = 54
    Width = 241
    Height = 211
    TabOrder = 4
    object Label2: TLabel
      Left = 16
      Top = 8
      Width = 200
      Height = 18
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1082#1085#1080#1075#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object AddSurname: TEdit
      Left = 16
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 0
      Text = #1040#1074#1090#1086#1088
    end
    object AddTitle: TEdit
      Left = 16
      Top = 67
      Width = 121
      Height = 21
      TabOrder = 1
      Text = #1053#1072#1079#1074#1072#1085#1080#1077
    end
    object AddYear: TEdit
      Left = 16
      Top = 94
      Width = 121
      Height = 21
      TabOrder = 2
      Text = #1043#1086#1076' '#1080#1079#1076#1072#1085#1080#1103
    end
    object AddPublishing: TEdit
      Left = 16
      Top = 121
      Width = 121
      Height = 21
      TabOrder = 3
      Text = #1048#1079#1076#1072#1090#1077#1083#1100#1089#1090#1074#1086
    end
    object AddPages: TEdit
      Left = 16
      Top = 148
      Width = 121
      Height = 21
      TabOrder = 4
      Text = #1057#1090#1088#1072#1085#1080#1094
    end
    object ButtonAdd: TButton
      Left = 38
      Top = 175
      Width = 163
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsItalic]
      ParentFont = False
      TabOrder = 5
      OnClick = ButtonAddClick
    end
  end
  object PanelSearch: TPanel
    Left = 24
    Top = 271
    Width = 241
    Height = 124
    TabOrder = 5
    object Edit1: TEdit
      Left = 38
      Top = 59
      Width = 163
      Height = 21
      TabOrder = 0
    end
    object ButtonSearch: TButton
      Left = 38
      Top = 86
      Width = 163
      Height = 25
      Caption = #1055#1086#1080#1089#1082
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsItalic]
      ParentFont = False
      TabOrder = 1
      OnClick = ButtonSearchClick
    end
    object ComboBox1: TComboBox
      Left = 16
      Top = 10
      Width = 210
      Height = 21
      TabOrder = 2
      Text = #1042#1099#1073#1077#1088#1080#1090#1077' '#1087#1088#1080#1085#1094#1080#1087' '#1087#1086#1080#1089#1082#1072
    end
  end
  object PanelOnePublishing: TPanel
    Left = 296
    Top = 289
    Width = 177
    Height = 87
    TabOrder = 6
    object EditPublishing: TEdit
      Left = 24
      Top = 10
      Width = 121
      Height = 21
      TabOrder = 0
      Text = #1048#1079#1076#1072#1090#1077#1083#1100#1089#1090#1074#1086
    end
    object ButtonBooks: TButton
      Left = 16
      Top = 45
      Width = 145
      Height = 28
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsItalic]
      ParentFont = False
      TabOrder = 1
      OnClick = ButtonBooksClick
    end
  end
  object SaveDialog1: TSaveDialog
    FileName = 
      '\\Mac\Home\Documents\Embarcadero\Studio\Projects\laba2\Win32\Deb' +
      'ug\f.txt'
    Left = 696
    Top = 24
  end
  object OpenDialog1: TOpenDialog
    FileName = 
      '\\Mac\Home\Documents\Embarcadero\Studio\Projects\laba2\Win32\Deb' +
      'ug\f.txt'
    Left = 696
    Top = 88
  end
end
