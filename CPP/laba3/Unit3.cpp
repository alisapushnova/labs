//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit3.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;
List lst;

void out(int index,TMemo* Memo1)
{
	int time = lst[index].get_time();
	Memo1->Lines->Text=Memo1->Lines->Text +
	lst[index].get_number() + "\t\t" + lst[index].get_type() +
	"\t\t" + lst[index].get_destination() + "\t\t\t";
	if ( time/60 == 0 ) Memo1->Lines->Text = Memo1->Lines->Text +
		"00:";
	else

	if ( time/60 < 10) Memo1->Lines->Text = Memo1->Lines->Text +
		"0" + IntToStr( time/60 )+":";
	else
		Memo1->Lines->Text = Memo1->Lines->Text +
		IntToStr(time/60) + ":";

	if ( time%60 == 0 ) Memo1->Lines->Text = Memo1->Lines->Text;

	if ( time % 60 < 10 ) Memo1->Lines->Text = Memo1->Lines->Text +
		"0" + IntToStr(time%60) + "\n";
	else
		Memo1->Lines->Text = Memo1->Lines->Text +
		IntToStr( time % 60 ) + "\n";
}
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Button1Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Button2Click(TObject *Sender)
{
	try {
		Flight *t = new Flight();
		t->set_number(Edit1->Text);
		t->set_type(Edit2->Text);
		t->set_destination(Edit3->Text);
		t->set_time(StrToInt(Edit4->Text),StrToInt(Edit6->Text));
		lst.push_back(t);
	}
	catch (int i) {
		ShowMessage("");
	}
}
//---------------------------------------------------------------------------


void __fastcall TForm3::Button4Click(TObject *Sender)
{
	Memo1->Clear();
	Memo1->Lines->Text="Number\t\tType\t\tDestination\tTime\n";
	int size=lst.size;
	for(int i=0; i<size; i++)
	{
	 if (lst[i].get_destination()==Edit5->Text)
		out(i,Memo1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Button5Click(TObject *Sender)
{
  Memo1->Clear();
  Memo1->Lines->Text="Number\t\tType\t\tDestination\tTime\n";

  int size=lst.size;
  for(int i=0; i<size; i++)
  {
   if (lst[i].get_number()==Edit7->Text)
		out(i,Memo1);
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Button6Click(TObject *Sender)
{
  Memo1->Clear();
  Memo1->Lines->Text="Number\t\tType\t\tDestination\tTime\n";
  int size=lst.size;
  for(int i=0; i<size; i++)
  {
   int time=lst[i].get_time();
   if (time==StrToInt(Edit9->Text)*60+StrToInt(Edit8->Text))
		out(i,Memo1);
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Button7Click(TObject *Sender)
{
  int i=0;
  while(i<lst.size)
	if (lst[i].get_destination() == Edit10->Text && lst[i].get_number() == Edit11->Text)
	{
		lst.pop_index(i);
		i=0;
	}
	else i++;
}
//---------------------------------------------------------------------------


void __fastcall TForm3::Timer1Timer(TObject *Sender)
{
	if ( Edit1->Text == "" || Edit2->Text == "" || Edit3->Text == "" || Edit4->Text == "" || Edit6->Text == "" ) Button2->Enabled = false;
	else Button2->Enabled = true;
	if ( Edit5->Text == "" ) Button4->Enabled = false;
	else Button4->Enabled = true;
	if ( Edit7->Text == "" ) Button5->Enabled = false;
	else Button5->Enabled = true;
	if ( Edit9->Text == "" || Edit8->Text == "" ) Button6->Enabled = false;
	else Button6->Enabled = true;
	if ( Edit10->Text == "" && Edit11->Text == "" ) Button7->Enabled = false;
	else Button7->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Button3Click(TObject *Sender)
{
	Memo1->Clear();
	Edit1->Text = "";
	Edit2->Text = "";
	Edit3->Text = "";
	Edit4->Text = "";
	Edit5->Text = "";
	Edit6->Text = "";
	Edit7->Text = "";
	Edit8->Text = "";
	Edit9->Text = "";
	Edit10->Text = "";
    Edit11->Text = "";
	for ( int i = 0; i < lst.size; i++ )
		out(i,Memo1);
}
//---------------------------------------------------------------------------

