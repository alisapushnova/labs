// ---------------------------------------------------------------------------

#ifndef Unit4H
#define Unit4H
#include <StdCtrls.hpp>

class Flight {
private:
	int time;
	String number, type, destination, state;

public:
	void set_number(String number) {
		this->number = number;
	}

	String get_number() {
		return number;
	}

	void set_type(String type) {
		this->type = type;
	}

	String get_type() {
		return type;
	}

	void set_destination(String destination) {
		this->destination = destination;
	}

	String get_destination() {
		return destination;
	}

	void set_time(int s1, int s2) {
		time = s1 * 60 + s2;
	}

	int get_time() {
		return time;
	}

	String get_state() {
		return state;
	}

	void set_state(String state) {
		this->state = state;
	}
};

class List {
public:
	List();
	~List();

private:
	class Node {
	public:
		Node *pNext;
		Flight data;

		Node(Flight data = Flight(), Node *pNext = NULL) {
			this->data = data;
			this->pNext = pNext;
		}
	};

public:
	Node *head;
	int size;

	void pop_front();
	void pop_index(int index);
	void clear();
	void push_back(Flight *data);

	int get_size() {
		return size;
	}
	Flight &operator[](const int index);
};
// ---------------------------------------------------------------------------
#endif
