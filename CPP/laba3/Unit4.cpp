//---------------------------------------------------------------------------


#pragma hdrstop

#include "Unit4.h"
List::List()
{
   size = 0;
   head = NULL;
}
List::~List()
{
	clear();
}
void List::push_back(Flight *data)
{
   if ( head == NULL )
   {
	  head = new Node(*data);
   }
   else
   {
	  Node *curent = this->head;
	  while ( curent->pNext != NULL)
	  {
		curent = curent->pNext;
	  }
	  curent->pNext = new Node(*data);

   }
   size++;
}

Flight &List::operator[](const int index)
{
	int counter = 0;
	Node *curent = this->head;
	while (curent != NULL)
	{
		if(counter == index) return curent->data;
		curent=curent->pNext;
		counter++;
 	}
     
}

void List::pop_front()
{
   Node *temp = this->head;
   head = head->pNext;
   delete temp;
   size--;
}

void List::clear()
{
   while (size)
      pop_front();
}

void List::pop_index(int index)
{
   if (index == 0)
   {
	  pop_front();
   }
   else
   {
	  Node *curent = this->head;
	  for (int i = 0; i < index - 1; i++)
		 if (curent->pNext)
			curent = curent->pNext;
		 else
			return;

	  Node *temp = curent->pNext;
	  curent->pNext = temp->pNext;
	  delete temp;
	  size--;
   }

}
//---------------------------------------------------------------------------

#pragma package(smart_init)
