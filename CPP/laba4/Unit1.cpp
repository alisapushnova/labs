// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <string>
using namespace std;

#include "Unit1.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
Convert C;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner){
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	string sForEdit = AnsiString(Edit1->Text).c_str();
	Edit1->Clear();
	string result = C.ConvertToOPZ(sForEdit);
	Edit1->Text = result.c_str();
	C.CreateEdits(Form1);
}
// ---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	string result = AnsiString(Edit1->Text).c_str();
	Edit1->Text = FloatToStr(C.Calculation(result));
}
//---------------------------------------------------------------------------

