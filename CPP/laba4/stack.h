// ---------------------------------------------------------------------------

#ifndef stackH
#define stackH
#include <string>
using namespace std;

// ---------------------------------------------------------------------------
class Stack
{
	class Node
	{
	public:
		Node*next;
		char operand;

		Node(char operand, Node*next = NULL)
		{
			this->operand = operand;
			this->next = next;
		}
	};

	Node*head;
	public:
	Stack();
	void Push(char operand);
	char Pop();
	char Check();
};

#endif
