// ---------------------------------------------------------------------------
#include "stack.h"
#include "stackFloat.h"
#include <stdlib.h>
#ifndef ConvertH
#define ConvertH
// ---------------------------------------------------------------------------
#endif

class Convert
{
	TEdit **E;
	char masOfOperands[10];

	int size = 0;

public:
	string ConvertToOPZ(string s);
	float Calculation(string s);
	void CreateEdits(TForm*Form1);
};
