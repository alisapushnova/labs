// ---------------------------------------------------------------------------

#ifndef stackFloatH
#define stackFloatH
#include <string>
using namespace std;

// ---------------------------------------------------------------------------
class StackFloat
{
	class Node
	{
	public:
		Node*next;
		float operand;

		Node(float operand, Node*next = NULL)
		{
			this->operand = operand;
			this->next = next;
		}
	};

	Node*head;
	public:
	StackFloat();
	void Push(float operand);
	float Pop();
	float Check();
};

#endif
