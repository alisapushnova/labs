// ---------------------------------------------------------------------------

#pragma hdrstop

#include "stack.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

Stack::Stack()
{
	head = NULL;
}

void Stack::Push(char operand)
{
	Node *current;
	current = new Node(operand);
	if (head == NULL)
	{
		head = current;
	}
	else {
		current->next = head;
		head = current;
	}
}

char Stack::Pop()
{
	Node *current = head;
	head = current->next;
	char s = current->operand;
	delete current;
	return s;
}

char Stack::Check()
{
	Node *current = head;
	char s = current->operand;
	return s;
}

