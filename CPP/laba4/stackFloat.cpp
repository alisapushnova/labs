// ---------------------------------------------------------------------------

#pragma hdrstop

#include "stackFloat.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

StackFloat::StackFloat()
{
	head = NULL;
}

void StackFloat::Push(float operand)
{
	Node *current;
	current = new Node(operand);
	if (head == NULL)
	{
		head = current;
	}
	else {
		current->next = head;
		head = current;
	}
}

float StackFloat::Pop()
{
	Node *current = head;
	head = current->next;
	float s = current->operand;
	delete current;
	return s;
}

float StackFloat::Check()
{
	Node *current = head;
	float s = current->operand;
	return s;
}

