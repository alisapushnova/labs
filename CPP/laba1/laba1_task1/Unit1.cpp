﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "math.h"
#include "Figure.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "Trapeze.h"

Figure *F
   //	= new Circle(0,0,0)
	;
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	F = GenerateFigureByChoosenFigureTypeRadioGroupButton();

	CircleSettingsPanel -> Visible = true;

	Edit4->Text = FloatToStr(F->GetSquare());

	Edit5->Text = FloatToStr(F->GetPerimeter());

	F->Draw(Canvas);
}
//---------------------------------------------------------------------------
 Figure* TForm1::GenerateFigureByChoosenFigureTypeRadioGroupButton()
 {
	if (FigureTypeRadioGroup->ItemIndex == 0)
	{
	   return new Circle(StrToInt(Edit1->Text),
						 StrToInt(Edit2->Text),
						 StrToInt(Edit3->Text));
	}
	else if (FigureTypeRadioGroup->ItemIndex == 1)
	{
		return new Rectangle1(StrToInt(Edit1->Text),
							  StrToInt(Edit1->Text),
							  StrToInt(Edit2->Text),
							  StrToInt(Edit3->Text));
	}
	else if (FigureTypeRadioGroup->ItemIndex == 2)
	{
		return new Triangle(StrToInt(Edit1->Text),
							StrToInt(Edit2->Text),
							StrToInt(Edit3->Text),
							StrToInt(Edit6->Text));
	}
	else if (FigureTypeRadioGroup->ItemIndex == 3)
	{
		return new Trapeze(StrToInt(Edit1->Text),
							StrToInt(Edit2->Text),
							StrToInt(Edit3->Text));
	}

 }
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	Edit1->Text = IntToStr(StrToInt(Edit1->Text) + 5);
	Button1Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	Edit1->Text = IntToStr(StrToInt(Edit1->Text) - 5);
	Button1Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
	Timer1 -> Enabled = true;
	Edit2->Text = "50";
	Edit3->Text = "50";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	  Edit2->Text = IntToStr(StrToInt(Edit2->Text) + 5);
	  Edit3->Text = IntToStr(StrToInt(Edit3->Text) + 5);
	  Button1Click(Sender);

	  Timer1 -> Enabled = StrToInt(Edit2->Text) < 400;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FigureTypeRadioGroupClick(TObject *Sender)
{
	if (FigureTypeRadioGroup->ItemIndex == 0)
	{
		Button1->Caption = "Построить круг";
	}
	else if (FigureTypeRadioGroup->ItemIndex == 1)
	{
		Button1->Caption = "Построить квадрат";
	}
	else if (FigureTypeRadioGroup->ItemIndex == 2)
	{
		Button1->Caption = "Построить треугольник";
	}
	else if (FigureTypeRadioGroup->ItemIndex == 3)
	{
		Button1->Caption = "Построить трапецию";
	}
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button5Click(TObject *Sender)
{
	Timer2 -> Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer2Timer(TObject *Sender)
{
	int curr = StrToInt( Edit6->Text );
	if (curr >= 360)
	{
		Edit6->Text = "0";
		Timer2->Enabled = false;
	}
	else
	{
		Edit6->Text = IntToStr ( StrToInt( Edit6->Text ) + 3 );
	}

	F->rotateAngle = StrToInt( Edit6->Text );
	F->Draw(Canvas);
}
//---------------------------------------------------------------------------


