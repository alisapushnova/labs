//---------------------------------------------------------------------------

#ifndef TriangleH
#define TriangleH
//---------------------------------------------------------------------------
#include "Figure.h"
#include "math.h"

class Triangle : public Figure
{
public:
	int radius;

	Triangle(int r, int x, int y, int angle);

	double GetSquare() override
	{
		return 1.5 * radius * sqrt(3) * radius;
	}
	double GetPerimeter() override
	{
		return 3 * sqrt(3) * radius;
	}

	void Draw(TCanvas *Canvas) override;

	double getX(int delta = 0)
	{
		return radius * sin(M_PI / 180 * (rotateAngle + delta));
	}

	double getY(int delta = 0)
	{
		return radius * cos(M_PI / 180 * (rotateAngle + delta));
	}
};

#endif
