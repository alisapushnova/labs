//---------------------------------------------------------------------------

#ifndef RectangleH
#define RectangleH
//---------------------------------------------------------------------------

#include "Figure.h"

class Rectangle1 : public Figure
{
public:
	int pa, pb;

	Rectangle1(int a, int b, int x, int y);

	double GetSquare() override
	{
		return pa*pb;
	}
	double GetPerimeter() override
	{
		return 2 * (pa+pb);
	}

	void Draw(TCanvas *Canvas) override;
};








#endif
