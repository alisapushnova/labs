//---------------------------------------------------------------------------

#pragma hdrstop

#include "Rectangle.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
	Rectangle1::Rectangle1(int a, int b, int x, int y)
	{
		pa = a;
		pb = b;
		positionX = x;
		positionY = y;
	}

    	void Rectangle1::Draw(TCanvas *Canvas)
	{
		Erase(Canvas);
		Canvas->Pen->Color = clBlue;
		Canvas->Rectangle(positionX - pa,
						  positionY - pa,
						  positionX + pa,
						  positionY + pa);
	}
