//---------------------------------------------------------------------------

#pragma hdrstop

#include "Circle.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

	Circle::Circle(int r, int x, int y )
	{
		radius = r;
		positionX = x;
		positionY = y;
	}

    	void Circle::Draw(TCanvas *Canvas)
	{
		Erase(Canvas);
		Canvas->Pen->Color = clBlue;
		Canvas->Ellipse(positionX - radius,
						positionY - radius,
						positionX + radius,
						positionY + radius);
	}
