//---------------------------------------------------------------------------

#ifndef CircleH
#define CircleH
//---------------------------------------------------------------------------

#include "Figure.h"
#include "math.h"

class Circle : public Figure
{
public:
	int radius;

	Circle(int r, int x, int y );

	double GetSquare() override
	{
		return radius * radius * M_PI;
	}
	double GetPerimeter() override
	{
		return 2 * radius * M_PI;
	}

	void Draw(TCanvas *Canvas) override;

};


#endif
