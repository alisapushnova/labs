//---------------------------------------------------------------------------

#pragma hdrstop

#include "Figure.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

	void Figure::Erase(TCanvas *Canvas)
	{
		Canvas->Brush->Color = clWhite;
		Canvas->FillRect ( Canvas->ClipRect );
	}
