//---------------------------------------------------------------------------

#pragma hdrstop

#include "Triangle.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
	Triangle::Triangle(int r, int x, int y, int angle)
	{
		radius = r;
		positionX = x;
		positionY = y;
		angle = 0;
		rotateAngle = angle;
	}

	void Triangle::Draw(TCanvas *Canvas)
	{
		Erase(Canvas);
		Canvas->MoveTo(positionX + getX(), positionY - getY());

		Canvas->Pen->Color = clGreen;
		Canvas->LineTo(positionX + getX(120), positionY - getY(120));

		Canvas->Pen->Color = clYellow;
		Canvas->LineTo(positionX + getX(240), positionY - getY(240));

		Canvas->Pen->Color = clRed;
		Canvas->LineTo(positionX + getX(), positionY - getY());
	}
