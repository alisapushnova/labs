//---------------------------------------------------------------------------

#pragma hdrstop

#include "Trapeze.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
	Trapeze::Trapeze(int r, int x, int y)
	{
		radius = r;
		positionX = x;
		positionY = y;
	}

	void Trapeze::Draw(TCanvas *Canvas)
	{
		Erase(Canvas);
		Canvas->MoveTo(positionX, positionY);

		Canvas->Pen->Color = clGreen;
		Canvas->LineTo(positionX + radius, positionY);

		Canvas->Pen->Color = clYellow;
		Canvas->LineTo(positionX + radius / 2, positionY - radius * sqrt(3) / 2);

		Canvas->Pen->Color = clRed;
		Canvas->LineTo(positionX - radius / 2, positionY - radius * sqrt(3) / 2);

		Canvas->Pen->Color = clGreen;
		Canvas->LineTo(positionX - radius, positionY);

		Canvas->Pen->Color = clRed;
		Canvas->LineTo(positionX, positionY);
	}
