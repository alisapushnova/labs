

#ifndef FigureH
#define FigureH
//---------------------------------------------------------------------------
class Figure
{
public:
	int positionX, positionY;
    int rotateAngle;

	virtual double GetSquare() = 0;
	virtual double GetPerimeter() = 0;

	virtual void Draw(TCanvas *Canvas)= 0;

	void Erase(TCanvas *Canvas);
};
	 #endif
