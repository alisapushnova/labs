//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "Figure.h"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *Button1;
	TEdit *Edit1;
	TEdit *Edit2;
	TEdit *Edit3;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TEdit *Edit4;
	TButton *Button2;
	TButton *Button3;
	TLabel *Label6;
	TLabel *Label7;
	TEdit *Edit5;
	TTimer *Timer1;
	TPanel *CircleSettingsPanel;
	TButton *Button4;
	TRadioGroup *FigureTypeRadioGroup;
	TButton *Button5;
	TLabel *Label8;
	TEdit *Edit6;
	TTimer *Timer2;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FigureTypeRadioGroupClick(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Timer2Timer(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
    Figure* GenerateFigureByChoosenFigureTypeRadioGroupButton();
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------






#endif
