//---------------------------------------------------------------------------

#ifndef TrapezeH
#define TrapezeH
//---------------------------------------------------------------------------
#include "Figure.h"
#include "math.h"

class Trapeze : public Figure
{
public:
	int radius;

	Trapeze(int r, int x, int y);

	double GetSquare() override
	{
		return 3 * radius * radius * sqrt(3) / 4;
	}
	double GetPerimeter() override
	{
		return 5 * radius;
	}

	void Draw(TCanvas *Canvas) override;
};

#endif
