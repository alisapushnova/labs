object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 511
  ClientWidth = 797
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 19
  object Label1: TLabel
    Left = 508
    Top = 167
    Width = 173
    Height = 19
    Caption = #1055#1086#1089#1090#1088#1086#1077#1085#1080#1077' '#1092#1080#1075#1091#1088#1099':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 499
    Top = 203
    Width = 10
    Height = 19
    Caption = 'R'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 575
    Top = 205
    Width = 9
    Height = 16
    Caption = 'X'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 647
    Top = 205
    Width = 9
    Height = 16
    Caption = 'Y'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 516
    Top = 245
    Width = 194
    Height = 29
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100' '#1082#1088#1091#1075
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 517
    Top = 199
    Width = 52
    Height = 27
    TabOrder = 1
    Text = '100'
  end
  object Edit2: TEdit
    Left = 590
    Top = 199
    Width = 51
    Height = 27
    TabOrder = 2
    Text = '200'
  end
  object Edit3: TEdit
    Left = 662
    Top = 201
    Width = 51
    Height = 27
    TabOrder = 3
    Text = '200'
  end
  object CircleSettingsPanel: TPanel
    Left = 469
    Top = 288
    Width = 273
    Height = 212
    ParentCustomHint = False
    Color = clSkyBlue
    Ctl3D = True
    ParentBackground = False
    ParentCtl3D = False
    TabOrder = 4
    Visible = False
    object Label7: TLabel
      Left = 13
      Top = 76
      Width = 70
      Height = 19
      Caption = #1055#1077#1088#1080#1084#1077#1090#1088':'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 10
      Top = 11
      Width = 63
      Height = 19
      Caption = #1055#1083#1086#1097#1072#1076#1100':'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 14
      Top = 116
      Width = 129
      Height = 19
      Caption = #1052#1072#1089#1096#1090#1072#1073#1080#1088#1086#1074#1072#1085#1080#1077':'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 13
      Top = 45
      Width = 129
      Height = 19
      Caption = #1059#1075#1086#1083' '#1090#1088#1077#1091#1075#1086#1083#1100#1085#1080#1082#1072':'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object Edit5: TEdit
      Left = 168
      Top = 73
      Width = 73
      Height = 27
      MaxLength = 6
      ReadOnly = True
      TabOrder = 0
    end
    object Edit4: TEdit
      Left = 168
      Top = 7
      Width = 73
      Height = 27
      MaxLength = 6
      ReadOnly = True
      TabOrder = 1
    end
    object Button2: TButton
      Left = 159
      Top = 141
      Width = 75
      Height = 25
      Caption = '+'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 9
      Top = 141
      Width = 75
      Height = 25
      Caption = '-'
      TabOrder = 3
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 135
      Top = 172
      Width = 130
      Height = 25
      Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 9
      Top = 172
      Width = 120
      Height = 25
      Caption = #1042#1088#1072#1097#1077#1085#1080#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = Button5Click
    end
    object Edit6: TEdit
      Left = 168
      Top = 40
      Width = 73
      Height = 27
      MaxLength = 6
      TabOrder = 6
      Text = '0'
    end
  end
  object FigureTypeRadioGroup: TRadioGroup
    Left = 499
    Top = 8
    Width = 198
    Height = 153
    Caption = #1058#1080#1087' '#1092#1080#1075#1091#1088#1099':'
    ItemIndex = 0
    Items.Strings = (
      #1050#1088#1091#1075
      #1050#1074#1072#1076#1088#1072#1090
      #1058#1088#1077#1091#1075#1086#1083#1100#1085#1080#1082
      #1058#1088#1072#1087#1077#1094#1080#1103)
    TabOrder = 5
    OnClick = FigureTypeRadioGroupClick
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 16
    OnTimer = Timer1Timer
    Left = 754
    Top = 199
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 16
    OnTimer = Timer2Timer
    Left = 754
    Top = 255
  end
end
