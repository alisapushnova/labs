//---------------------------------------------------------------------------

#ifndef ellipseH
#define ellipseH
//---------------------------------------------------------------------------

  class Ellipse
{
public:
	int positionX, positionY;

	virtual void DrawFace(TCanvas *Canvas)= 0;

	virtual void DrawEyse(TCanvas *Canvas)= 0;

	void Erase(TCanvas *Canvas)
	{
		Canvas->Brush->Color = clWhite;
		Canvas->FillRect ( Canvas->ClipRect );
	}

};

#endif

