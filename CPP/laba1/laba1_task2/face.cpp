//---------------------------------------------------------------------------

#pragma hdrstop

#include "face.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
Face::Face(int r, int x, int y )
{
		radius = r;
		positionX = x;
		positionY = y;
}
void Face::DrawFace(TCanvas *Canvas)
{
		Canvas->Pen->Color = clBlue;
		Canvas->Ellipse(positionX - radius,
						positionY + radius,
						positionX + radius,
						positionY - radius);
}
void Face::DrawEyse(TCanvas *Canvas)
{
		Canvas->Pen->Color = clRed;
		Canvas->Ellipse(positionX - radius,
						positionY + radius,
						positionX + radius,
						positionY - radius);
}
