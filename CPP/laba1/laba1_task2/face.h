//---------------------------------------------------------------------------

#ifndef faceH
#define faceH
//---------------------------------------------------------------------------
#include "Ellipse.h"
#include "math.h"

class Face : public Ellipse
{
public:
	int radius;
	Face(int r, int x, int y );

	void DrawFace(TCanvas *Canvas) override;

	void DrawEyse(TCanvas *Canvas) override;
};


#endif
