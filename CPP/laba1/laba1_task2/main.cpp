//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "main.h"
#include "ellipse.h"
#include "face.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm2 *Form2;
Face *face = NULL;
Face *face1 = NULL;
Face *face2 = NULL;
Face *face3 = NULL;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button1Click(TObject *Sender)
{
	face = new Face(150, 200, 200);
	face->DrawFace(Canvas);

	face1 = new Face(20, posXLeft, posYLeft);
	face1->DrawEyse(Canvas);

	face2 = new Face(20, posXRight, posYRight);
	face2->DrawEyse(Canvas);

	face3 = new Face(radDown, posXDown, posYDown);
	face3->DrawEyse(Canvas);

	Canvas->MoveTo(200,200);
	Canvas->LineTo(210,220);
	Canvas->LineTo(190,220);
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button2Click(TObject *Sender)
{
	Timer1 -> Enabled = true;

}
//---------------------------------------------------------------------------

void __fastcall TForm2::Timer1Timer(TObject *Sender)
{
	face1->Erase(Canvas);
	posXLeft -= 5;
	posXRight += 5;
	radDown += 2;

	Button1Click(Sender);

	if(posXLeft == 100 && posXRight == 300)
	{
		Timer1->Enabled = false;
		Timer2->Enabled = true;
	}

}
//---------------------------------------------------------------------------

void __fastcall TForm2::Timer2Timer(TObject *Sender)
{
	face1->Erase(Canvas);
	posXLeft += 5;
	posXRight -= 5;
	radDown -= 2;

	Button1Click(Sender);

	if(posXLeft == 140 && posXRight == 260)
	{
		Timer2->Enabled = false;
	}



}
//---------------------------------------------------------------------------

