//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TListBox *Table;
	TButton *ButonAdd;
	TEdit *EditAdd;
	TButton *ButtonDelete;
	TButton *ButtonClear;
	TButton *ButtonExit;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TEdit *EditNumberElement;
	TEdit *EditStart;
	TEdit *EditEnd;
	TButton *ButtonGenerate;
	TButton *ButtonAddElement;
	TButton *ButtonStart;
	void __fastcall ButonAddClick(TObject *Sender);
	void __fastcall ButtonDeleteClick(TObject *Sender);
	void __fastcall ButtonClearClick(TObject *Sender);
	void __fastcall ButtonExitClick(TObject *Sender);
	void __fastcall ButtonGenerateClick(TObject *Sender);
	void __fastcall ButtonAddElementClick(TObject *Sender);
	void __fastcall ButtonStartClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
