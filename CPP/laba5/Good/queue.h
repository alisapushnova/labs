//---------------------------------------------------------------------------

#ifndef queueH
#define queueH
#include <vcl.h>
class List
{
public:
	List();
	~List();

	class Node
	{
	public:
		Node *next;
		Node *prev;
		int number;
	};

	Node *head,*tail,*after;
	int size;
	void Add(int number);
	void Delete();
	void Clear();
	void Show(TListBox *LB);
};
//---------------------------------------------------------------------------
#endif
