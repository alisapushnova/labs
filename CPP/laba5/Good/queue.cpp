// ---------------------------------------------------------------------------

#pragma hdrstop

#include "queue.h"
#include <vcl.h>
#include <memory>
#include <string.h>
// ---------------------------------------------------------------------------

List::List() {
	size = 0;
	head = tail = NULL;
}

List::~List() {
	Clear();
}

void List::Add(int number)
{
	Node *temp = new Node;
	temp->number = number;
	temp->next = NULL;
	if (head != NULL) {
		temp->prev = tail;
		tail->next = temp;
		tail = temp;
	}

	else {
		temp->prev = NULL;
		head = tail = temp;
	}
	size++;
}

void List::Clear() {
	while (size)
		Delete();
}

void List::Delete() {
	if (size > 0) {
		Node *temp = head;
		head = head->next;
		delete temp;
		size--;
	}
}

void List::Show(TListBox *LB)
{
	LB->Items->Clear();
	Node *current = head;
	while (current) {
		LB->Items->Add(current->number);
		current = current->next;
	}
	delete current;
}




