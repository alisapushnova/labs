// ---------------------------------------------------------------------------

#pragma hdrstop

#include "another_queue.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

void Another_queue::Move(int begin, int start, int end)
{
	Node *current = head;
	int count_elements = 1;
	Node *begin_poiner;
	Node *start_poiner;
	Node *end_poiner;

	while (current)
	{
		if (count_elements == begin)
		{
		   begin_poiner = current;
		}
		else if (count_elements == start)
		{
		   start_poiner = current;
		}
		else if (count_elements == end)
		{
		   end_poiner = current;
		}
		count_elements++;
		current = current->next;
	}

	start_poiner -> prev -> next = end_poiner -> next;
	end_poiner -> next -> prev = start_poiner -> prev;

	end_poiner -> next = begin_poiner -> next;
	begin_poiner -> next -> prev = end_poiner;

	begin_poiner -> next = start_poiner;
	start_poiner -> prev =  begin_poiner;


}
