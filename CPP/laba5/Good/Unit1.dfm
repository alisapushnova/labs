object Form1: TForm1
  Left = 740
  Top = 478
  Caption = 'Form1'
  ClientHeight = 246
  ClientWidth = 608
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 176
    Top = 134
    Width = 81
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1101#1083#1077#1084#1077#1085#1090#1072
  end
  object Label2: TLabel
    Left = 176
    Top = 164
    Width = 37
    Height = 13
    Caption = #1053#1072#1095#1072#1083#1086
  end
  object Label3: TLabel
    Left = 176
    Top = 191
    Width = 31
    Height = 13
    Caption = #1050#1086#1085#1077#1094
  end
  object Table: TListBox
    Left = 8
    Top = 8
    Width = 145
    Height = 225
    ItemHeight = 13
    TabOrder = 0
  end
  object ButonAdd: TButton
    Left = 176
    Top = 24
    Width = 129
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 1
    OnClick = ButonAddClick
  end
  object EditAdd: TEdit
    Left = 327
    Top = 24
    Width = 130
    Height = 21
    TabOrder = 2
  end
  object ButtonDelete: TButton
    Left = 176
    Top = 55
    Width = 129
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 3
    OnClick = ButtonDeleteClick
  end
  object ButtonClear: TButton
    Left = 327
    Top = 55
    Width = 130
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 4
    OnClick = ButtonClearClick
  end
  object ButtonExit: TButton
    Left = 470
    Top = 208
    Width = 130
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 5
    OnClick = ButtonExitClick
  end
  object EditNumberElement: TEdit
    Left = 263
    Top = 131
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object EditStart: TEdit
    Left = 263
    Top = 158
    Width = 121
    Height = 21
    TabOrder = 7
  end
  object EditEnd: TEdit
    Left = 263
    Top = 185
    Width = 121
    Height = 21
    TabOrder = 8
  end
  object ButtonGenerate: TButton
    Left = 471
    Top = 24
    Width = 129
    Height = 25
    Caption = #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
    TabOrder = 9
    OnClick = ButtonGenerateClick
  end
  object ButtonAddElement: TButton
    Left = 431
    Top = 129
    Width = 169
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1084#1077#1088' '#1101#1083#1077#1084#1077#1085#1090#1072
    TabOrder = 10
    OnClick = ButtonAddElementClick
  end
  object ButtonStart: TButton
    Left = 431
    Top = 164
    Width = 169
    Height = 25
    Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' '#1087#1088#1086#1075#1088#1072#1084#1084#1091
    TabOrder = 11
    OnClick = ButtonStartClick
  end
end
