// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Tree.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
Tree tree;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner)
{
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	StringGrid1->ColWidths[0] = 50;
	StringGrid1->Cells[0][0] = "�����:";
	StringGrid1->Cells[1][0] = "�������:";
	StringGrid1->Cells[2][0] = "���:";
	StringGrid1->ColWidths[3] = 140;
	StringGrid1->Cells[3][0] = "����� ��������:";
	for (int i = 1; i <= 100; i++)
	{
		StringGrid1->Cells[0][i] = IntToStr(i);
	}
	StringGrid1->Cells[3][1] = "3035059";
	StringGrid1->Cells[3][2] = "3034504";
	StringGrid1->Cells[3][3] = "4945431";
	StringGrid1->Cells[3][4] = "2084715";
	StringGrid1->Cells[3][5] = "2822189";
	StringGrid1->Cells[1][1] = "�������";
	StringGrid1->Cells[2][1] = "�����";
	StringGrid1->Cells[1][2] = "��������";
	StringGrid1->Cells[2][2] = "������";
	StringGrid1->Cells[1][3] = "�������";
	StringGrid1->Cells[2][3] = "������";
	StringGrid1->Cells[1][4] = "������";
	StringGrid1->Cells[2][4] = "����";
	StringGrid1->Cells[1][5] = "������";
	StringGrid1->Cells[2][5] = "����";

}
// ---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	int i = 1;
	while (StringGrid1->Cells[1][i] != "" || StringGrid1->Cells[2][i] != "" ||
		StringGrid1->Cells[3][i] != "")
	{
		String surname = StringGrid1->Cells[1][i];
		String name = StringGrid1->Cells[2][i];
		int passportId = StrToInt(StringGrid1->Cells[3][i]);
		tree.AddItem(surname, name, passportId, &(tree.root));
		i++;
	}
	Button2->Enabled = false;
	Button1->Enabled = true;
	Button3->Enabled = true;
	Button4->Enabled = true;
	tree.ViewTree(tree.root, -1);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	String surname, name;
	int passportId, i = 1;
	try
	{
		surname = Edit1->Text;
		name = Edit2->Text;
		passportId = StrToInt(Edit3->Text);
		tree.AddItem(surname, name, passportId, &(tree.root));
	}
	catch (...)
	{
		ShowMessage("�������� ����");
		Edit1->Clear();
		Edit2->Clear();
		Edit3->Clear();
		return;
	}
	TreeView1->Items->Clear();
	tree.ViewTree(tree.root, -1);
	Edit1->Clear();
	Edit2->Clear();
	Edit3->Clear();
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	tree.Delete(tree.root, StrToInt(Edit4->Text));
	Edit4->Clear();
	TreeView1->Items->Clear();
	tree.ViewTree(tree.root, -1);

}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	tree.Balance(tree.root);
	TreeView1->Items->Clear();
	tree.ViewTree(tree.root, -1);
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
	if (tree.root == NULL)
	{
		Memo1->Clear();
		Memo1->Lines->Add("������ ������!");
	}
	else {
		Memo1->Clear();
		Form1->Memo1->Lines->Add("***������ �����***");
		tree.LeftOrder(Memo1, tree.root);
		Memo1->Lines->Add("");
		Form1->Memo1->Lines->Add("***�������� �����***");
		tree.ReverseOrder(Memo1, tree.root);
		Memo1->Lines->Add("");
		Form1->Memo1->Lines->Add("***����� �� ����������� �����***");
		tree.InternalOrder(Memo1, tree.root);
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button6Click(TObject *Sender)
{
	Item*res = tree.Search(tree.root, StrToInt(Edit4->Text));
	if (res != NULL)
	{
		Memo1->Clear();
		Memo1->Lines->Add("������� ������!");
		Memo1->Lines->Add("�������: " + res->surname);
		Memo1->Lines->Add("���: " + res->name);
		Memo1->Lines->Add("����� ��������: " + IntToStr(res->passportId));
	}
	else
	{
		Memo1->Clear();
		Memo1->Lines->Add("���, �� ������ �������� ���!");
	}
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::AnswerClick(TObject *Sender)
{
	int res = tree.SearchElem(tree.root, 0);
	Memo1->Clear();
	Memo1->Lines->Add("���������: " + IntToStr(res));

}
// ---------------------------------------------------------------------------


