object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 446
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 416
    Height = 240
    TabOrder = 0
    object StringGrid1: TStringGrid
      Left = 8
      Top = 16
      Width = 397
      Height = 208
      ColCount = 4
      DefaultColWidth = 80
      DefaultRowHeight = 25
      RowCount = 100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      ParentFont = False
      TabOrder = 0
      ColWidths = (
        80
        80
        80
        59)
    end
  end
  object Panel2: TPanel
    Left = 476
    Top = 200
    Width = 242
    Height = 133
    TabOrder = 1
    object Label1: TLabel
      Left = 13
      Top = 16
      Width = 48
      Height = 13
      Caption = #1060#1072#1084#1080#1083#1080#1103':'
    end
    object Label2: TLabel
      Left = 14
      Top = 43
      Width = 23
      Height = 13
      Caption = #1048#1084#1103':'
    end
    object Label3: TLabel
      Left = 14
      Top = 72
      Width = 85
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
    end
    object Edit1: TEdit
      Left = 112
      Top = 13
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object Edit2: TEdit
      Left = 112
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object Edit3: TEdit
      Left = 112
      Top = 69
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object Button1: TButton
      Left = 64
      Top = 96
      Width = 121
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Enabled = False
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  object Panel3: TPanel
    Left = 255
    Top = 266
    Width = 185
    Height = 113
    TabOrder = 2
    object Button2: TButton
      Left = 32
      Top = 42
      Width = 121
      Height = 25
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1076#1077#1088#1077#1074#1086
      TabOrder = 0
      OnClick = Button2Click
    end
    object Button4: TButton
      Left = 32
      Top = 73
      Width = 121
      Height = 25
      Caption = #1057#1073#1072#1083#1072#1085#1089#1080#1088#1086#1074#1072#1090#1100
      Enabled = False
      TabOrder = 1
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 32
      Top = 11
      Width = 121
      Height = 25
      Caption = #1042#1099#1074#1077#1089#1090#1080' '#1076#1077#1088#1077#1074#1086
      TabOrder = 2
      OnClick = Button5Click
    end
  end
  object Panel5: TPanel
    Left = 476
    Top = 354
    Width = 242
    Height = 82
    TabOrder = 3
    object Label4: TLabel
      Left = 15
      Top = 17
      Width = 85
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
    end
    object Edit4: TEdit
      Left = 106
      Top = 14
      Width = 119
      Height = 21
      TabOrder = 0
    end
    object Button3: TButton
      Left = 15
      Top = 41
      Width = 95
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Enabled = False
      TabOrder = 1
      OnClick = Button3Click
    end
    object Button6: TButton
      Left = 130
      Top = 41
      Width = 95
      Height = 25
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 2
      OnClick = Button6Click
    end
  end
  object Answer: TButton
    Left = 255
    Top = 396
    Width = 185
    Height = 40
    Caption = #1063#1080#1089#1083#1086' '#1091#1079#1083#1086#1074' '#1089' '#1086#1076#1085#1080#1084' '#1087#1086#1090#1086#1084#1082#1086#1084
    TabOrder = 4
    OnClick = AnswerClick
  end
  object Memo1: TMemo
    Left = 439
    Top = 8
    Width = 313
    Height = 177
    ScrollBars = ssVertical
    TabOrder = 5
  end
  object TreeView1: TTreeView
    Left = 24
    Top = 263
    Width = 193
    Height = 177
    Indent = 19
    TabOrder = 6
  end
end
