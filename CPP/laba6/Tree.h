// ---------------------------------------------------------------------------

#ifndef TreeH
#define TreeH
#include <cstring>
#include "Unit1.h"
// ---------------------------------------------------------------------------
#endif

class Item
{
public:
	Item *left;
	Item *right;
	String name;
	String surname;
	int passportId;

	Item()
	{
	}

	Item(String surname, String name, int passportId)
	{
		this->name = name;
		this->surname = surname;
		this->passportId = passportId;
	}
};

class Tree
{
public:
	Item *root;

	Tree()
	{
		root = NULL;
	}

	Tree(Item *root)
	{
		this->root = root;
		root->left = root->right = NULL;
	}

	void AddItem(String surname, String name, int passportId, Item **node)
	{
		if ((*node) == NULL)
		{
			(*node) = new Item();
			(*node)->name = name;
			(*node)->surname = surname;
			(*node)->passportId = passportId;
			(*node)->left = (*node)->right = NULL;
		}
		else {
			if (passportId > (*node)->passportId)
			{
				AddItem(name, surname, passportId, &(*node)->right);
			}
			else if (passportId < (*node)->passportId)
			{
				AddItem(name, surname, passportId, &(*node)->left);
			}
			else {
				throw Exception
					("������� ���������� � ������ ��� ������������� ��������");
			}
		}
	}

	void ViewTree(Item *p, int kl)
	{
		if (p == NULL)
			return;
		if (kl == -1)
			Form1->TreeView1->Items->AddFirst(NULL,
			p->surname + "(" + IntToStr(p->passportId) + ")");
		else
			Form1->TreeView1->Items->AddChildFirst
				(Form1->TreeView1->Items->Item[kl],
			p->surname + "(" + IntToStr(p->passportId) + ")");
		kl++;
		ViewTree(p->left, kl);
		ViewTree(p->right, kl);
		kl--;
	}

	Item *Delete(Item* node, int val)
	{
		if (node == NULL)
			return node;
		if (val == node->passportId)
		{
			Item *temp;
			if (node->right == NULL)
			{
				temp = node->left;
			}
			else {
				Item* ptr = node->right;
				if (ptr->left == NULL)
				{
					ptr->left = node->left;
					temp = ptr;
				}
				else {
					Item* pmin = ptr->left;
					int count;
					while (pmin->left != NULL)
					{
						ptr = pmin;
						pmin = ptr->left;
					}
					++count;
					ptr->left = pmin->right;
					pmin->left = node->left;
					pmin->right = node->right;
					temp = pmin;
				}
			}
			if (node == root)
			{
				root = temp;
			}
			delete node;
			return temp;
		}
		else if (val < node->passportId)
		{
			node->left = Delete(node->left, val);
		}
		else
			node->right = Delete(node->right, val);
		return node;
	}

	void Balance(Item *item)
	{
		String _surname[1000];
		String _name[1000];
		int _passportId[1000];
		int amount;
		if (root == NULL)
		{
			throw Exception("������ ������");
		}
		amount = ArrayFill(root, 0, _surname, _name, _passportId);
		root = NULL;
		int middle = (0 + amount - 1) / 2;
		RebildTree(0, amount, _surname, _name, _passportId);
	}

	int ArrayFill(Item *begin, int k, String *_surname, String *_name,
		int *_passportId)
		{
		if (begin == NULL)
		{
			return k;
		}
		k = ArrayFill(begin->left, k, _surname, _name, _passportId);
		_surname[k] = begin->surname;
		_name[k] = begin->name;
		_passportId[k] = begin->passportId;
		k++;
		k = ArrayFill(begin->right, k, _surname, _name, _passportId);
		return k;
	}

	void RebildTree(int left, int right, String *_surname, String *_name,
		int *_passportId)
		{
		int middle = (left + right) / 2;
		if (left == right)
		{
			return;
		}
		AddItem(_surname[middle], _name[middle], _passportId[middle], &root);
		RebildTree(left, middle, _surname, _name, _passportId);
		RebildTree(middle + 1, right, _surname, _name, _passportId);
	}

	void LeftOrder(TMemo *Memo1, Item *node)
	{
		if (node)
		{
			Memo1->Lines->Add(node->surname + "(" +
				IntToStr(node->passportId) + ")");
			LeftOrder(Memo1, node->left);
			LeftOrder(Memo1, node->right);
		}
	}

	void ReverseOrder(TMemo *Memo1, Item *node)
	{
		if (node)
		{
			ReverseOrder(Memo1, node->left);
			ReverseOrder(Memo1, node->right);
			Memo1->Lines->Add(node->surname + "(" +
				IntToStr(node->passportId) + ")");
		}
	}

	void InternalOrder(TMemo *Memo1, Item *node)
	{
		if (node)
		{
			InternalOrder(Memo1, node->left);
			Memo1->Lines->Add(node->surname + "(" +
				IntToStr(node->passportId) + ")");
			InternalOrder(Memo1, node->right);
		}
	}

	Item *Search(Item *temp, int key)
	{
		if (temp == NULL)
			return temp;
		if (key > temp->passportId)
		{
			temp = Search(temp->right, key);
		}
		else if (key < temp->passportId)
		{
			temp = Search(temp->left, key);
		}
		else if (key == temp->passportId)
		{
			return temp;
		}
		else
			return NULL;
		return temp;
	}

	int SearchElem(Item*node, int k)
	{
		if (node == NULL)
		{
			return k;
		}
		k = SearchElem(node->left, k);
		if ((node->left == NULL && node->right != NULL) ||
			(node->right == NULL && node->left != NULL))
		{
			k++;
		}
		k = SearchElem(node->right, k);
		return k;
	}
};
